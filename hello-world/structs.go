package main

import "fmt"

type gorra struct {
	marca  string
	color  string
	precio float32
	plana  bool
}

func main() {
	var gorraNegra = gorra{
		marca:  "Nike",
		color:  "Negro",
		precio: 25.50,
		plana:  false,
	}

	fmt.Println(gorraNegra)

	var gorraVerde = gorra{
		marca:  "Adidas",
		color:  "Verde",
		precio: 20.19,
		plana:  true,
	}

	fmt.Println(gorraVerde)
}
