package main

import (
	"fmt"
)

func main() {
	//Variables
	var suma int = 8 + 9
	var resta int = 6 - 4
	var nombre string = "Alejandro "
	var apellidos string = "Alzate "

	//Variables
	pais := "Colombia"

	//Constantes
	const year int = 2021

	fmt.Println("Hello World! " + nombre + apellidos + pais)
	fmt.Println(suma)
	fmt.Println(resta)
	fmt.Println(year)
}
