package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var collection = getSession().DB("curso_go").C("movies")

func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}

	return session
}

func responseMovie(writer http.ResponseWriter, status int, result Movie) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(result)
}

func responseMovies(writer http.ResponseWriter, status int, results []Movie) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(results)
}

func Index(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "Hola mundo desde mi servidor web con Go")
}

func Contact(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "Esta es la página de contacto")
}

func MovieList(writer http.ResponseWriter, request *http.Request) {
	var results []Movie
	err := collection.Find(nil).All(&results)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Resultados: ", results)
	}

	responseMovies(writer, 200, results)
}

func MovieShow(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	movieId := params["id"]

	if !bson.IsObjectIdHex(movieId) {
		writer.WriteHeader(404)
		return
	}

	objectId := bson.ObjectIdHex(movieId)
	result := Movie{}
	err := collection.FindId(objectId).One(&result)

	if err != nil {
		writer.WriteHeader(404)
		return
	}

	responseMovie(writer, 200, result)
}

func MovieAdd(writer http.ResponseWriter, request *http.Request) {
	decoder := json.NewDecoder(request.Body)
	var result Movie
	err := decoder.Decode(&result)

	if err != nil {
		panic(err)
	}

	defer request.Body.Close()

	err = collection.Insert(result)
	if err != nil {
		writer.WriteHeader(500)
		return
	}

	responseMovie(writer, 200, result)
}

func MovieUpdate(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	movieId := params["id"]

	if !bson.IsObjectIdHex(movieId) {
		writer.WriteHeader(404)
		return
	}

	objectId := bson.ObjectIdHex(movieId)
	decoder := json.NewDecoder(request.Body)
	var movieData Movie
	err := decoder.Decode(&movieData)

	if err != nil {
		writer.WriteHeader(500)
		return
	}

	defer request.Body.Close()

	document := bson.M{"_id": objectId}
	change := bson.M{"$set": movieData}
	err = collection.Update(document, change)

	if err != nil {
		writer.WriteHeader(404)
		return
	}

	responseMovie(writer, 200, movieData)
}

type Message struct {
	Status  string `json: "status"`
	Message string `json: "message"`
}

func (this *Message) setStatus(data string) {
	this.Status = data
}

func (this *Message) setMessage(data string) {
	this.Message = data
}

func MovieRemove(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	movieId := params["id"]

	if !bson.IsObjectIdHex(movieId) {
		writer.WriteHeader(404)
		return
	}

	objectId := bson.ObjectIdHex(movieId)

	err := collection.RemoveId(objectId)

	if err != nil {
		writer.WriteHeader(404)
		return
	}

	// result := Message{"success", "La película con id: " + movieId + " ha sido eliminada correctamente"}
	message := new(Message)
	message.setStatus("success")
	message.setMessage("La película con id: " + movieId + " ha sido eliminada correctamente")

	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(message)
}
