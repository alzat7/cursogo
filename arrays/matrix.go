package main

import "fmt"

func main() {
	var peliculas [3][2]string
	peliculas[0][0] = "La verdad duele"
	peliculas[0][1] = "Mientras duermes"

	peliculas[1][0] = "Ciudadano ejemplar"
	peliculas[1][1] = "El señor de los anillos"

	peliculas[2][0] = "Gran torino"
	peliculas[2][1] = "Rápido y furioso"

	fmt.Println(peliculas)
}
