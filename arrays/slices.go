package main

import "fmt"

func main() {
	peliculas := []string{
		"La verdad duele",
		"Ciudadano ejemplar",
		"Gran torino",
		"Superman",
	}

	peliculas = append(peliculas, "Sin límites")
	peliculas = append(peliculas, "Camp Rock")

	fmt.Println(peliculas)
	fmt.Print("El array tiene una longitud de: ")
	fmt.Println(len(peliculas))
	fmt.Print("Los primeros 3 elementos son: ")
	fmt.Println(peliculas[0:3])
}
