package main

import "fmt"

func main() {
	var peliculas [3]string
	peliculas[0] = "La verdad duele"
	peliculas[1] = "Ciudadano ejemplar"
	peliculas[2] = "Gran torino"

	fmt.Println(peliculas)

	peliculas2 := [3]string{
		"La verdad duele",
		"Ciudadano ejemplar",
		"Gran torino",
	}

	fmt.Println(peliculas2)
}
