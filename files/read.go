package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	fmt.Println("Lector")

	file, fileError := ioutil.ReadFile("file.txt")
	showError(fileError)
	fmt.Println(string(file))
}

func showError(e error) {
	if e != nil {
		panic(e)
	}
}
