package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("Escritor")

	nuevoTexto := os.Args[1] + "\n"
	//escribir := ioutil.WriteFile("file.txt", nuevoTexto, 0777)

	file, err := os.OpenFile("file.txt", os.O_APPEND|os.O_WRONLY, 0777)
	showError(err)

	escribir, err := file.WriteString(nuevoTexto)
	fmt.Println(escribir)
	showError(err)

	file.Close()

	texto, fileError := ioutil.ReadFile("file.txt")
	showError(fileError)

	fmt.Println(string(texto))
}

func showError(e error) {
	if e != nil {
		panic(e)
	}
}
