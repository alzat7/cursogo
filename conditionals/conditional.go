package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	fmt.Println("***** MI PROGRAMA CON GO *****")

	fmt.Println("Hola " + os.Args[1] + " bienvenido al programa GO")

	edad, _ := strconv.Atoi(os.Args[2])

	if (edad >= 18 || edad == 17) && edad != 25 && edad != 99 {
		fmt.Println("Eres mayor de edad o tienes 17")
	} else if edad == 25 {
		fmt.Println("Tienes 25 años")
	} else if edad == 99 {
		fmt.Println("Eres demasiado viejo")
	} else {
		fmt.Println("Eres MENOR de edad")
	}
}
