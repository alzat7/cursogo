package main

import (
	"fmt"
	"time"
)

func main() {
	momento := time.Now().Weekday()

	switch momento {
	case 0:
		fmt.Println("Hoy es domingo")
	case 1:
		fmt.Println("Hoy es lunes")
	case 2:
		fmt.Println("Hoy es martes")
	case 3:
		fmt.Println("Hoy es miercoles")
	default:
		fmt.Println("Hoy es otro día de la semana")
	}
}
