package main

import (
	"fmt"
)

func main() {

	peliculas := []string{"Pelicula 1", "Rápido y furioso", "El club de la pelea", "Gran torino"}

	for i := 0; i < len(peliculas); i++ {
		if i%2 == 0 {
			fmt.Println("La pelicula "+peliculas[i]+" es par", i)
		} else {
			fmt.Println("La pelicula "+peliculas[i]+" es impar", i)
		}
	}

	fmt.Println("---------------------------")

	for _, pelicula := range peliculas {
		fmt.Println(pelicula)
	}
}
