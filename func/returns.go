package main

import "fmt"

func main() {
	fmt.Println(devolverTexto())
}

func holaMundo() string {
	return "Hola Mundo!"
}

func devolverTexto() (string, int) {
	return "Alejandro", 25
}
