package main

import "fmt"

func main() {
	var numero1 float32 = 10
	var numero2 float32 = 6

	var numero3 float32 = 44
	var numero4 float32 = 7

	holaMundo()
	calculdora(numero1, numero2)
	calculdora(numero3, numero4)
}

func holaMundo() {
	fmt.Println("Hola Mundo!")
}

func operacion(n1 float32, n2 float32, op string) float32 {
	var resultado float32
	if op == "+" {
		resultado = n1 + n2
	}

	if op == "-" {
		resultado = n1 - n2
	}

	if op == "*" {
		resultado = n1 * n2
	}

	if op == "/" {
		resultado = n1 / n2
	}

	return resultado
}

func calculdora(numero1 float32, numero2 float32) {
	//Suma
	fmt.Print("La suma es: ")
	fmt.Println(operacion(numero1, numero2, "+"))

	//Resta
	fmt.Print("La resta es: ")
	fmt.Println(operacion(numero1, numero2, "-"))

	//Multiplicacion
	fmt.Print("La multiplicacion es: ")
	fmt.Println(operacion(numero1, numero2, "*"))

	//Division
	fmt.Print("La division es: ")
	fmt.Println(operacion(numero1, numero2, "/"))
}
